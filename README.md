1. ec2 setup

install below on ec2
  
 **java**
  
sudo apt-get update
sudo apt install openjdk-17-jre-headless -y
java --version

sudo apt-get remove openjdk-17-jre-headless -y

 **maven**
 
sudo apt update
sudo apt-get install maven -y
mvn --version

sudo apt-get remove maven -y
 
 **docker**

sudo apt-get update
sudo apt install docker.io -y
sudo docker --version

sudo systemctl start docker
sudo systemctl enable docker
sudo systemctl status docker

sudo apt-get remove docker docker-engine docker.io

give the permission to docker.sock to perform the operation on docker

command - chmod 777 /var/run/docker.sock

after stop and start server again give the permission
 
----------------------------------------------------------------------------------------------------------

2.connect runner

goto :- Peoject > settings > CICD > runner >

  install runner before that check the Architecture
  command - dpkg --print-architecture 
  and select ruuner according to architecture
  go to ruuner copy command and paste into ec2 for connection
  give the proper information like
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):[https://gitlab.com/]:- hit enter
Enter the registration token:[GR1348941bkm4m-mAhWAUZRfjzuJu]:- hit enter
Enter a description for the runner:[ip-172-31-35-203]:- hit enter
Enter tags for the runner (comma-separated):- give any tag name remember we have to give that name inside yml file (ec2,server)
Enter optional maintenance note for the runner:- hit enter
Enter an executor: docker+machine, kubernetes, instance, custom, shell, parallels, virtualbox, ssh, docker, docker-windows, docker-autoscaler:- shell

goto runner >  click on edit option > and check run without tag --if you want to run withut tag

----------------------------------------------------------------------------------------------------------

3.Shell profile loading - move to root before doing that operation - sudo su

To troubleshoot this error, check 
command - vi /home/gitlab-runner/.bash_logout
For example, if the .bash_logout file has a script section like the following, 
comment it out and restart the pipeline:

before :-

if [ "$SHLVL" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
fi

after :-

#if [ "$SHLVL" = 1 ]; then
#  		[ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
#fi

----------------------------------------------------------------------------------------------------------

step 4-

goto gitlab account profile create access personal token

gitlab token generation - goto Preferences > Access Tokens > Add new Token > Add Token name
> Remove expiration date > check all checkboxex > create personal access token

and add variable like  $GITLAB_USER -p $GITLAB_TOKEN

user gitlab user name for GITLAB_USER
access personal token for GITLAB_TOKEN
	
----------------------------------------------------------------------------------------------------------

step 5

    GITLAB_REGISTRY: registry.gitlab.com/sachin-borde/springdockerimagepush
	for above variable goto gitlab registry( Deploy > container registry>)
	copy all 3 command for pushing the image

create .gitlab-ci.yaml file

{
variables:
  REGISTRY: registry.gitlab.com/raj-mandale/springdockerimagepush
  TAG: "$CI_COMMIT_SHORT_SHA-$CI_PIPELINE_ID"

stages:
    - build
    - build_image
    - push_image_to_registry

build:
    stage: build
    image:  maven:3.8.3-openjdk-17
    script:
        - echo "Building app..."
        - mvn install
        - echo "Finished building the app."
    artifacts:
        expire_in: 1 week
        paths:
            - target/helloworld.jar
    tags:
        - ec2
        - server

build_image:
    stage: build_image
    script:
        - echo "building image"
        - docker info
        - docker build . -t $REGISTRY:$TAG
        - echo "Finished building image."
    tags:
        - ec2
        - server

deploy_push:
    stage: push_image_to_registry
    script:
        - echo "building con"
        - docker login registry.gitlab.com -u "$CI_USER" -p "$CI_TOKEN"
        - docker push $REGISTRY:$TAG
        - echo "Finished building con."
    tags:
        - ec2
        - server


}

check the image in 
Deploy > Container Registry > 

----------------------------------------------------------------------------------------------------------
